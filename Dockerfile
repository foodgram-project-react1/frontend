# Строим приложение на основе официального образа Node.js
FROM node:14 AS build

# Установите рабочую директорию
WORKDIR /usr/src/app

# Копируем файлы package.json и package-lock.json (если есть)
COPY package*.json ./

# Устанавливаем зависимости
RUN npm install

# Копируем исходный код
COPY . .

# Собираем приложение
RUN npm run build

# Запускаем приложение на основе образа nginx
FROM nginx:stable-alpine

# Копируем собранное приложение из фазы build в папку nginx
COPY --from=build /usr/src/app/build /usr/share/nginx/html

# Экспонируем порт
EXPOSE 80

# Запускаем nginx
CMD ["nginx", "-g", "daemon off;"]
